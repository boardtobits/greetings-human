﻿using UnityEngine;
using System.Collections;

public enum RaceDiff {
	Easy,
	Medium,
	Hard
}

public class AlienRace {

	public string name {get;set;}
	public int name1, name2, name3;
	public bool genderSpecific {get;set;}
	public RaceDiff difficulty {get;set;}

	//appearance
	public int head {get;set;}
	public int eyes {get;set;}
	public int mouth {get;set;}
	public int nose {get;set;}
	public int skinColor {get;set;}
	public int noseColor {get;set;}
	public int clothesColor {get;set;}

	
	//customs
	public int greeting {get;set;}
	public int farewell {get;set;}
	public int greetingF {get;set;}
	public int farewellF {get;set;}
	public int insult {get;set;}
	public int insult2 {get;set;}

	public AlienRace(RaceDiff diff){
		difficulty = diff;

		//name alien race
		name1 = Random.Range (0, GameManager.ins.db.raceStart.Length);
		name = GameManager.ins.db.raceStart[name1];
		if (Random.value < 0.5f){
			name2 = Random.Range (0, GameManager.ins.db.raceMid.Length);
			name += GameManager.ins.db.raceMid[name2];
		} else {
			name2 = -1;
		}
		name3 = Random.Range (0, GameManager.ins.db.raceEnd.Length);
		name += GameManager.ins.db.raceEnd[name3];

		//check for duplicate names

		//create initial greetings
		greeting = Random.Range(0, GameManager.ins.db.gestures.Length);
		farewell = Random.Range(0, GameManager.ins.db.gestures.Length);

		//check for gender-specific greetings
		if ((difficulty == RaceDiff.Medium && Random.value < 0.3f) || (difficulty == RaceDiff.Hard && Random.value < 0.7f)){
			genderSpecific = true;
			do {
				greetingF = Random.Range(0, GameManager.ins.db.gestures.Length);
				farewellF = Random.Range(0, GameManager.ins.db.gestures.Length);
			} while (greetingF == greeting && farewellF == farewell);
		} else {
			genderSpecific = false;
			greetingF = greeting;
			farewellF = farewell;
		}

		//check for first insult
		if ((difficulty == RaceDiff.Easy && Random.value < 0.05f) || (difficulty == RaceDiff.Medium && Random.value < 0.3f) || (difficulty == RaceDiff.Hard && Random.value < 0.7f)){
			do {
				insult = Random.Range(0, GameManager.ins.db.gestures.Length);
			} while (insult == greeting || insult == farewell || insult == greetingF || insult == farewellF);
		} else {
			insult = -1;
		}

		//check for second insult
		if (insult != -1 && ((difficulty == RaceDiff.Medium && Random.value < 0.1f) || (difficulty == RaceDiff.Hard && Random.value < 0.3f))){
			do {
				insult2 = Random.Range(0, GameManager.ins.db.gestures.Length);
			} while (insult2 == greeting || insult2 == farewell || insult2 == greetingF || insult2 == farewellF || insult2 == insult);
		} else {
			insult2 = -1;
		}

		//create appearance
		head = Random.Range (0, GameManager.ins.db.heads.Length);
		eyes = Random.Range (0, GameManager.ins.db.eyes.Length);
		if (Random.value < 0.1f) {
			mouth = -1;
		} else {
			mouth = Random.Range (0, GameManager.ins.db.mouths.Length);
		}
		if (Random.value < 0.1f) {
			nose = -1;
		} else {
			nose = Random.Range (0, GameManager.ins.db.noses.Length);
		}
		skinColor = Random.Range (0, GameManager.ins.db.skin.Length);
		if (nose != -1 && Random.value < 0.5f) {
			noseColor = Random.Range (0, GameManager.ins.db.skin.Length);
		} else {
			noseColor = -1;
		}
		clothesColor = Random.Range (0, GameManager.ins.db.clothes.Length);

	}

	public string WriteGreeting (){
		return GameManager.ins.db.gestures[greeting].gName;
	}

	public string WriteGreetingF (){
		return GameManager.ins.db.gestures[greetingF].gName;
	}

	public string WriteFarewell (){
		return GameManager.ins.db.gestures[farewell].gName;
	}

	public string WriteFarewellF (){
		return GameManager.ins.db.gestures[farewellF].gName;
	}

	public string WriteInsult (){
		return GameManager.ins.db.gestures[insult].gName;
	}

	public string WriteInsult2 (){
		return GameManager.ins.db.gestures[insult2].gName;
	}

	public int CurrentNeed(MeetingState mS){
		if (mS == MeetingState.Greeting && GameManager.isMale){
			return greeting;
		} else if (mS == MeetingState.Greeting){
			return greetingF;
		} else if (mS == MeetingState.Farewell && GameManager.isMale){
			return farewell;
		} else if (mS == MeetingState.Farewell){
			return farewellF;
		}
		return -1;
	}

	public int OffGender(MeetingState mS){
		if (mS == MeetingState.Greeting && GameManager.isMale){
			return greetingF;
		} else if (mS == MeetingState.Greeting){
			return greeting;
		} else if (mS == MeetingState.Farewell && GameManager.isMale){
			return farewellF;
		} else if (mS == MeetingState.Farewell){
			return farewell;
		}
		return -1;
	}



}
