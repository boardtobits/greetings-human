﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AlienRender : MonoBehaviour {

	public Image body, head, eyes, mouth, nose;

	public void Render(AlienRace race){
		body.color = GameManager.ins.db.clothes[race.clothesColor];
		head.sprite = GameManager.ins.db.heads[race.head];
		head.color = GameManager.ins.db.skin[race.skinColor];
		eyes.sprite = GameManager.ins.db.eyes[race.eyes];
		if (race.nose >= 0){
			nose.gameObject.SetActive(true);
			nose.sprite = GameManager.ins.db.noses[race.nose];
			if (race.noseColor >= 0){
				nose.color = GameManager.ins.db.skin[race.noseColor];
			} else {
				nose.color = GameManager.ins.db.skin[race.skinColor] * 0.9f;
			}
		} else {
			nose.gameObject.SetActive(false);
		}
		if (race.mouth >= 0){
			mouth.gameObject.SetActive(true);
			mouth.sprite = GameManager.ins.db.mouths[race.mouth];
			if (race.mouth == 3){
				mouth.color = GameManager.ins.db.skin[race.skinColor] * 1.1f;
			} else {
				mouth.color = Color.white;
			}
		} else {
			mouth.gameObject.SetActive(false);
		}


	}
}
