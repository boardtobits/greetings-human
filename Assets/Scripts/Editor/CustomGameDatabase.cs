﻿using UnityEngine;
using UnityEditor;
using System;

public class CustomGameDatabase {
	
	[MenuItem("Assets/Create/Game Database")]
	public static void CreateAsset(){
		CustomAssetUtility.CreateAsset<GameDatabase>();
	}
}
