﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VoteUI : MonoBehaviour {

	public Text yea, nay, abs, und;
	public GameObject nextButton;
	public float delay;

	public void UpdateVote(){
		StartCoroutine(ShowVotes());
	}

	IEnumerator ShowVotes(){
		yield return new WaitForSeconds(delay);
		und.text = GameManager.undecided.ToString();
		yield return new WaitForSeconds(delay);
		yea.text = GameManager.yeaVote.ToString();
		nay.text = GameManager.nayVote.ToString();
		abs.text = GameManager.abstainVote.ToString();
		yield return new WaitForSeconds(delay);
		GameManager.CheckEndGame();
	}

	public void ExitVote(){
		nextButton.SetActive(false);
		GameManager.BeginLevel();
	}
}
