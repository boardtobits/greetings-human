﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Gesture {

	public string gName;
	public Sprite gSprite;
}
