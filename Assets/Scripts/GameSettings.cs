﻿using UnityEngine;
using System.Collections;

public static class GameSettings {

	public static int numRaces = 30;

	public static float meetingTimer = 20f;
	public static float farewellTimer = 10f;
	public static float UIdelay = 1f;

	public static int threshold = 0;
	public static DiplomatMood yeaReq = DiplomatMood.Friendly;

	public static void SetCasual(){
		meetingTimer = 30f;
		farewellTimer = 15f;
		threshold = 0;
		yeaReq = DiplomatMood.Friendly;
	}

	public static void SetNormal(){
		meetingTimer = 20f;
		farewellTimer = 8f;
		threshold = 0;
		yeaReq = DiplomatMood.Friendly;
	}

	public static void SetHard(){
		meetingTimer = 15f;
		farewellTimer = 5f;
		threshold = 0;
		yeaReq = DiplomatMood.Impressed;
	}

}
