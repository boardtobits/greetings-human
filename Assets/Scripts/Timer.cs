﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

	public Text display;

	// Update is called once per frame
	void Update () {
		UpdateDisplay();
		if (!GameManager.timerRunning){
			gameObject.SetActive(false);
		}
	}

	void UpdateDisplay(){
		if (GameManager.timeRemaining > 0f){
			string leadin;
			if (GameManager.timeRemaining < 10f){
				leadin = "00:0";
			} else {
				leadin = "00:";
			}
			display.text = leadin + GameManager.timeRemaining.ToString("F2");
		}

	}
}
