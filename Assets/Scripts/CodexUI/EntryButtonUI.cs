﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EntryButtonUI : MonoBehaviour {

	public Text title;
	public int index {get;set;}

	public void DisplayEntry(){
		GameManager.ins.codexUI.entryDisplay.gameObject.SetActive(true);
		GameManager.ins.codexUI.entryDisplay.Display(index);
	}

}
