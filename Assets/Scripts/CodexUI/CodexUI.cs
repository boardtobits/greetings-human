﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CodexUI : MonoBehaviour {

	public GridLayoutGroup entries;
	public EntryButtonUI entryPrefab;
	public EntryUI entryDisplay;

	void Awake(){
		entryDisplay.gameObject.SetActive(false);
	}

	void Update(){
		if (GameManager.timeRemaining == 0f && GameManager.timerRunning){
			GameManager.ins.meetingUI.gameObject.SetActive(true);
			if (GameManager.state == GameState.Research){
				GameManager.SetState(GameState.Meeting);
				GameManager.BeginMeeting();
			}
			GameManager.TryGesture(GameManager.currentRace.insult);
			gameObject.SetActive(false);
		}
	}

	public void Display(string key = null){
		foreach (Transform entry in entries.transform){
			Destroy(entry.gameObject);
		}
		for (int i = 0; i < GameManager.codex.races.Length; i++){
			AlienRace race = GameManager.codex.races[i];
			if (key == null){
				EntryButtonUI newEntry = Instantiate(entryPrefab) as EntryButtonUI;
				newEntry.gameObject.transform.SetParent(entries.transform, false);
				newEntry.title.text = race.name;
				newEntry.index = i;
			}
		}
	}

	public void ExitCodex(){
		GameManager.ins.meetingUI.gameObject.SetActive(true);
		if (GameManager.state == GameState.Research){
			GameManager.SetState(GameState.Meeting);
			GameManager.BeginMeeting();
		}
		GameManager.ins.codexUI.gameObject.SetActive(false);
	}

}
