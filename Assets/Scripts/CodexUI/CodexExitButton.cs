﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CodexExitButton : MonoBehaviour {

	public Text text;

	void OnEnable(){
		if (GameManager.state == GameState.Research){
			text.text = "MEETING";
		} else {
			text.text = "EXIT";
		}
	}
}
