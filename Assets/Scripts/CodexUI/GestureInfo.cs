﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GestureInfo : MonoBehaviour {

	public Text title;
	public Text gestureName;

	public void Display(string type, string gest){
		title.text = type;
		gestureName.text = gest;
	}
}
