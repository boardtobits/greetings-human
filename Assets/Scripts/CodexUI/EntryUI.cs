﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EntryUI : MonoBehaviour {

	public GestureInfo infoPrefab;
	public Transform greetings, farewells, insults;
	public AlienRender portrait;

	public Text raceName;

	public void Display(int index){
		Clear ();
		AlienRace race = GameManager.codex.races[index];
		raceName.text = race.name;
		portrait.Render(race);
		if (race.genderSpecific == false){
			GestureInfo newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("GREETING", race.WriteGreeting());
			newBlock.gameObject.transform.SetParent(greetings);
			newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("FAREWELL", race.WriteFarewell());
			newBlock.gameObject.transform.SetParent(farewells);
		} else {
			GestureInfo newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("GREETING (MALE)", race.WriteGreeting());
			newBlock.gameObject.transform.SetParent(greetings);
			newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("GREETING (FEMALE)", race.WriteGreetingF());
			newBlock.gameObject.transform.SetParent(greetings);
			newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("FAREWELL (MALE)", race.WriteFarewell());
			newBlock.gameObject.transform.SetParent(farewells);
			newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("FAREWELL (FEMALE)", race.WriteFarewellF());
			newBlock.gameObject.transform.SetParent(farewells);
		}
		if (race.insult >= 0){
			GestureInfo newBlock = Instantiate(infoPrefab) as GestureInfo;
			newBlock.Display ("CAUTION: INSULT", race.WriteInsult());
			newBlock.gameObject.transform.SetParent(insults);
			if (race.insult2 >= 0){
				newBlock = Instantiate(infoPrefab) as GestureInfo;
				newBlock.Display ("CAUTION: INSULT", race.WriteInsult2());
				newBlock.gameObject.transform.SetParent(insults);
			}
		}
	}

	public void Back(){
		Clear();
		gameObject.SetActive(false);
	}

	void Clear(){
		foreach (Transform info in greetings){
			Destroy(info.gameObject);
		}
		foreach (Transform info in farewells){
			Destroy(info.gameObject);
		}
		foreach (Transform info in insults){
			Destroy(info.gameObject);
		}
	}
}
