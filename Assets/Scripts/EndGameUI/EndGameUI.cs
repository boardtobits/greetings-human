﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGameUI : MonoBehaviour {

	public Text story, epilogue;

	public void Display(int tally){
		int threshold = GameSettings.threshold;
		if (tally < threshold && tally > 0) {
			story.text = "Despite your best efforts, the council has voted against humanity with only " + tally.ToString() + " votes in our favor.";
			epilogue.text = "Humanity is allowed to live, but we are relegated to terraforming duties on the smelliest planets.";
		} else if (tally < threshold && tally == 0){
			story.text = "The vote ended in a draw, a firm testament against even-numbered councils.";
			epilogue.text = "Humanity is abandoned to its fate, restricted to planet Earth and its satellite for eternity.";
		} else if (tally < threshold){
			story.text = "Nice work. You managed to tick off the council with " + Mathf.Abs (tally).ToString() + " votes against us.";
			epilogue.text = "Earth is obliterated into a tiny ball of ash. Everyone blamed you.";
		} else if (tally == 10){
			story.text = "You managed to woo all 10 members of the council, even the really unpleasant ones!";
			epilogue.text = "Earth is invited into the Alliance and you are given a seat on the council, fixing that awful even-number problem.";
		} else {
			story.text = "The council voted in favor of humanity by " + tally.ToString () + " votes.";
			epilogue.text = "You represented humanity well, and proved we are not the monsters the universe thinks us to be.";
		}
	}

	public void ToMenu(){
		GameManager.ins.mainmenuUI.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}
}
