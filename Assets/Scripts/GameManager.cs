﻿using UnityEngine;
using System.Collections;

public enum GameState{
	Menu,
	Assistant,
	Research,
	Meeting,
	EndLevel
}

public enum MeetingState{
	None,
	Greeting,
	SmallTalk,
	Farewell
}

public enum DiplomatMood{
	Insulted,
	Bothered,
	Neutral,
	Friendly,
	Impressed
}

public class GameManager : MonoBehaviour {

	//setup data
	public static GameManager ins;
	public GameDatabase db;
	public static AlienCodex codex;

	//screens
	public MainMenuUI mainmenuUI;
	public CodexUI codexUI;
	public AssistantUI assistUI;
	public MeetingUI meetingUI;
	public VoteUI voteUI;
	public EndGameUI endgameUI;

	//audio
	public AudioManager audioMgr;
	
	//gameplay data
	public static bool timerRunning;
	public static float timeRemaining;
	public static int threshold;
	public static int currentLevel;
	public static AlienRace currentRace;
	public static AlienRace[] councilRaces;
	public static AlienRace assistantRace;
	public static bool isMale;
	public static GameState state;
	public static MeetingState meetState;
	public static DiplomatMood currentMood;
	public static int yeaVote, nayVote, abstainVote, undecided;

	public static void SetState(GameState newState){
		state = newState;
	}

	public static void SetMeetState(MeetingState newState){
		meetState = newState;
	}

	public static void ImproveMood(){
		ins.audioMgr.PlaySFX(1);
		if (currentMood == DiplomatMood.Impressed){
			return;
		}
		currentMood++;
		if (ins.meetingUI.gameObject.activeSelf){
			ins.meetingUI.moodTracker.UpdateHead();
		}
	}

	public static void ReduceMood(){
		ins.audioMgr.PlaySFX(3);
		if (currentMood == DiplomatMood.Insulted){
			return;
		}
		currentMood--;
		if (ins.meetingUI.gameObject.activeSelf){
			ins.meetingUI.moodTracker.UpdateHead();
		}
	}


	void Awake (){
		ins = this;
		mainmenuUI.gameObject.SetActive(true);
		codexUI.gameObject.SetActive(false);
		assistUI.gameObject.SetActive(false);
		meetingUI.gameObject.SetActive(false);
		endgameUI.gameObject.SetActive(false);
		state = GameState.Menu;
		meetState = MeetingState.None;
	}

	void Update(){
		if (timerRunning && timeRemaining == 0f){
			timerRunning = false;
		} else if (timerRunning){
			timeRemaining = Mathf.Clamp (timeRemaining - Time.deltaTime, 0f, float.MaxValue);
		}
	}

	public static void StartGame(int diff){
		if (diff == 0){
			GameSettings.SetCasual();
		} else if (diff == 1){
			GameSettings.SetNormal();
		} else {
			GameSettings.SetHard();
		}
		ins.mainmenuUI.gameObject.SetActive(false);
		GameManager.codex = new AlienCodex();
		assistantRace = codex.races[Random.Range (0, codex.races.Length)];
		councilRaces = new AlienRace[10];
		currentLevel = 1;
		yeaVote = 0;
		nayVote = 0;
		abstainVote = 0;
		undecided = 10;
		ins.voteUI.yea.text = yeaVote.ToString ();
		ins.voteUI.nay.text = nayVote.ToString ();
		ins.voteUI.abs.text = abstainVote.ToString ();
		ins.voteUI.und.text = undecided.ToString ();
		ins.codexUI.Display();
		BeginLevel();
	}

	public static void BeginLevel(){
		ins.voteUI.gameObject.SetActive(false);
		RaceDiff diffCheck;
		if (currentLevel < 4){
			diffCheck = RaceDiff.Easy;
		} else if (currentLevel < 8) {
			diffCheck = RaceDiff.Medium;
		} else {
			diffCheck = RaceDiff.Hard;
		}
		do {
			currentRace = codex.races[Random.Range(0, codex.races.Length)];
		} while (currentRace.difficulty != diffCheck || CouncilDuplicate());
		councilRaces[councilRaces.Length - undecided] = currentRace;
		if (Random.value < 0.5f){
			isMale = !isMale;
		}
		state = GameState.Assistant;
		ins.assistUI.gameObject.SetActive(true);
		ins.assistUI.Display();
	}

	public static bool CouncilDuplicate(){
		for (int i = 0; i < councilRaces.Length; i++){
			if (councilRaces[i] != null && councilRaces[i].name == currentRace.name){
				return true;
			}
		}
		return false;
	}

	public static void BeginMeeting(){
		ins.meetingUI.codexButton.SetActive(true);
		ins.meetingUI.timer.SetActive(true);
		currentMood = DiplomatMood.Neutral;
		meetState = MeetingState.Greeting;
		ins.meetingUI.DisplayOptions();
		ins.meetingUI.moodTracker.UpdateHead();
		ins.meetingUI.alien.Render(currentRace);
	}

	public static void TryGesture(int index){
		if (index == currentRace.CurrentNeed(meetState)){
			ImproveMood();
		} else if (index == currentRace.insult || index == currentRace.insult2){
			ReduceMood();
		} else {
			ins.audioMgr.PlaySFX(2);
		}
		if (meetState == MeetingState.Greeting){
			meetState = MeetingState.Farewell;
			timeRemaining = Mathf.Max (timeRemaining, GameSettings.farewellTimer);
			Debug.Log ("time set to " + timeRemaining);
			timerRunning = true;
			ins.meetingUI.DisplayOptions();
		} else {
			meetState = MeetingState.None;
			state = GameState.EndLevel;
			ins.meetingUI.ClearOptions();
			ins.meetingUI.Goodbye();
			timerRunning = false;
			ins.audioMgr.PlayLobby();
			ins.StartCoroutine (GameManager.EndLevel());
		}
	}

	public static IEnumerator EndLevel(){
		ins.meetingUI.codexButton.SetActive(false);
		float timer = 0;
		while (timer < GameSettings.UIdelay * 2){
			timer += Time.deltaTime;
			yield return null;
		}
		ins.meetingUI.gameObject.SetActive(false);
		AdjustVotes();
		Debug.Log ("votes adjusted");
		ins.voteUI.gameObject.SetActive(true);
		ins.voteUI.UpdateVote();
	}

	public static void AdjustVotes(){
		if (currentMood >= GameSettings.yeaReq){
			yeaVote++;
		} else if (currentMood < DiplomatMood.Neutral){
			nayVote++;
		} else {
			abstainVote++;
		}
		undecided--;
	}

	public static void CheckEndGame(){
		if (yeaVote - nayVote < GameSettings.threshold && yeaVote - nayVote + undecided < GameSettings.threshold){
			EndGame();
		} else if (undecided == 0){
			EndGame();
		} else {
			currentLevel++;
			ins.voteUI.nextButton.SetActive(true);
		}
	}

	static void EndGame(){
		ins.voteUI.gameObject.SetActive(false);
		ins.endgameUI.gameObject.SetActive(true);
		int tally = yeaVote - nayVote;
		ins.endgameUI.Display(tally);
	}
}
