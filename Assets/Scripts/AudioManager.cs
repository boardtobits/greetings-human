﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioSource bgm;
	public AudioSource sfx;

	public void PlaySFX(int choice){
		sfx.clip = GameManager.ins.db.sfx[choice];
		sfx.loop = false;
		sfx.Play();
	}

	public void PlayLobby(){
		bgm.clip = GameManager.ins.db.lobby;
		bgm.loop = true;
		bgm.Play();
	}

	public void PlayMission(){
		bgm.clip = GameManager.ins.db.mission[Random.Range(0, GameManager.ins.db.mission.Length)];
		sfx.loop = true;
		bgm.Play();
	}

}
