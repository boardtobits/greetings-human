﻿using UnityEngine;
using System.Collections;

public class AlienCodex {

	public AlienRace[] races;
	int easy, medium, hard;

	public AlienCodex(){
		races = new AlienRace[GameSettings.numRaces];
		for (int i = 0; i < races.Length; i++){
			races[i] = new AlienRace((RaceDiff)Random.Range (0, 3));
		}
	}


}
