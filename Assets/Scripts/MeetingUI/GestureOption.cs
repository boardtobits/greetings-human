﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GestureOption : MonoBehaviour {

	public Text title;
	public int index{get;set;}

	public void SelectGesture(){
		GameManager.TryGesture(index);
	}
}
