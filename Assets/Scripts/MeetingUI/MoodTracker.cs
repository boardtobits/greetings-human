﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoodTracker : MonoBehaviour {

	public Image head;

	public void UpdateHead(){
		head.sprite = GameManager.ins.db.moods[(int)GameManager.currentMood];
	}
}
