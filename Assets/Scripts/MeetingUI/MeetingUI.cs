﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MeetingUI : MonoBehaviour {

	public Transform options;
	public GestureOption optionPrefab;
	public Text story;
	public MoodTracker moodTracker;
	public AlienRender alien;
	public GameObject codexButton;
	public GameObject timer;

	public void Update(){
		if (GameManager.timeRemaining == 0f && GameManager.timerRunning){
			GameManager.TryGesture(GameManager.currentRace.insult);
		}
	}

	public void GoToCodex(){
		GameManager.ins.codexUI.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public void ClearOptions(){
		foreach (Transform option in options){
			Destroy (option.gameObject);
		}
	}

	public void DisplayOptions(){
		ClearOptions();
		string gender = "";
		if (GameManager.isMale){
			gender += "male";
		} else {
			gender += "female";
		}
		string gestType = "";
		if (GameManager.meetState == MeetingState.Greeting){
			gestType = "greeting";
		} else {
			gestType = "farewell";
		}
		story.text = "The " + gender + " " + GameManager.currentRace.name + " ambassador awaits your " + gestType + ".";

		List<int> optionList = new List<int>();
		//set correct gesture
		optionList.Add(GameManager.currentRace.CurrentNeed(GameManager.meetState));
		//set off-gender (if applicable)
		if (GameManager.currentRace.genderSpecific && optionList[0] != GameManager.currentRace.OffGender(GameManager.meetState) && Random.value < 0.5f){
			optionList.Add(GameManager.currentRace.OffGender(GameManager.meetState));
		}
		//set insult(s) (if applicable)
		if (GameManager.currentRace.insult >= 0){
			optionList.Add(GameManager.currentRace.insult);
			if (GameManager.currentRace.insult2 >= 0){
				optionList.Add(GameManager.currentRace.insult2);
			}
		}
		//set random others
		while (optionList.Count < 5){
			int randomOption = Random.Range (0, GameManager.ins.db.gestures.Length);
			while (optionList.Contains(randomOption)){
				randomOption = Random.Range (0, GameManager.ins.db.gestures.Length);
			}
			optionList.Add(randomOption);
		}
		int[] shuffledOptions = new int[optionList.Count];
		for (int i = 0; i < shuffledOptions.Length; i++){
			shuffledOptions[i] = optionList[Random.Range (0, optionList.Count)];
			optionList.Remove(shuffledOptions[i]);
		}
		foreach (int option in shuffledOptions){
			GestureOption newOption = Instantiate(optionPrefab);
			newOption.transform.SetParent(options, false);
			newOption.index = option;
			//newOption.title.text = option.ToString();
			newOption.title.text = GameManager.ins.db.gestures[option].gName;
		}
	}

	public void Goodbye(){
		story.text = "The " + GameManager.currentRace.name + " ambassador bids you a good day.";
	}


}
