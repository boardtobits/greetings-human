﻿using UnityEngine;
using System.Collections;

public class GameDatabase : ScriptableObject {

	//elements of alien race name
	public string[] raceStart, raceMid, raceEnd;

	//elements of diplomat name
	public string[] firstName, lastName;

	//list of gestures
	public Gesture[] gestures;

	//list of alien parts
	public Sprite[] heads, eyes, mouths, noses;

	//list alien colors
	public Color[] skin, clothes;

	//mood images
	public Sprite[] moods;

	//audio
	public AudioClip lobby;
	public AudioClip[] mission;
	public AudioClip[] sfx;

}
