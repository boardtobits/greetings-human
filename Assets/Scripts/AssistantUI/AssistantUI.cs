﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AssistantUI : MonoBehaviour {

	public Text convoIntro, convoRace, convoConc;
	public AlienRender alien;
	bool rendered;

	public void Display(){
		if (!rendered){
			alien.Render(GameManager.assistantRace);
			rendered = true;
		}
		if (GameManager.currentLevel == 1){
			convoIntro.text = "Welcome, human. For your first contact, you'll be meeting with the";
		} else if (GameManager.currentMood < DiplomatMood.Neutral){
			convoIntro.text = "You'll need to do better than that, ambassador. Next on the list is the";
		} else if (GameManager.currentMood < GameSettings.yeaReq){
			convoIntro.text = "Well, at least you didn't embarass yourself. Now it's time to meet the";
		} else {
			convoIntro.text = "Well done! But don't get lazy, next you're meeting the";
		}
		convoRace.text = GameManager.currentRace.name + " ambassador.";
		convoConc.text = "Your next meeting begins in " + GameSettings.meetingTimer.ToString() + " seconds.";
	}

	public void GoToCodex(){
		GameManager.SetState(GameState.Research);
		GameManager.timeRemaining = GameSettings.meetingTimer;
		GameManager.timerRunning = true;
		GameManager.ins.audioMgr.PlayMission();
		GameManager.ins.codexUI.gameObject.SetActive(true);
		GameManager.ins.assistUI.gameObject.SetActive(false);
	}
}
