﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkipButton : MonoBehaviour {

	public Text text;
	public Image button;

	public void FadeIn(){
		StartCoroutine(FadeButtonIn());
	}

	IEnumerator FadeButtonIn(){
		while (text.color.a < 1){
			float newA = Mathf.Min(1f, text.color.a + Time.deltaTime * 0.5f);
			button.color = new Color (button.color.r, button.color.g, button.color.b, newA * 0.6f);
			text.color = new Color (text.color.r, text.color.g, text.color.b, newA);
			yield return null;
		}
	}

	public void LoadGame(){
		StopAllCoroutines();
		Application.LoadLevel(1);
	}
}
