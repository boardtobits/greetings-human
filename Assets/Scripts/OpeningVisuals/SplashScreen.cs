﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	public Image logo;
	public Text story, bold;
	public SkipButton skipButton;

	void Awake(){
		StartCoroutine(Opening());
	}

	IEnumerator Opening(){
		yield return new WaitForSeconds(2.5f);
		while (logo.color.a > 0){
			float newA = Mathf.Max(0f, logo.color.a - Time.deltaTime);
			logo.color = new Color (logo.color.r, logo.color.g, logo.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(1.0f);
		skipButton.FadeIn();
		story.text = "In 2037, humanity made first contact with an alien species.";
		while (story.color.a < 1){
			float newA = Mathf.Min(1f, story.color.a + Time.deltaTime * 2);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(3.25f);
		while (story.color.a > 0){
			float newA = Mathf.Min(1f, story.color.a - Time.deltaTime);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(1.0f);
		bold.text = "It did not go well.";
		while (bold.color.a < 1){
			float newA = Mathf.Min(1f, bold.color.a + Time.deltaTime);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(2f);
		while (bold.color.a > 0){
			float newA = Mathf.Min(1f, bold.color.a - Time.deltaTime * 2);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		story.text = "Earth launched a pre-emptive attack.";
		while (story.color.a < 1){
			float newA = Mathf.Min(1f, story.color.a + Time.deltaTime * 2);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(2.0f);
		while (story.color.a > 0){
			float newA = Mathf.Min(1f, story.color.a - Time.deltaTime);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		story.text = "It would later be called the Welcome Wagon Massacre.";
		while (story.color.a < 1){
			float newA = Mathf.Min(1f, story.color.a + Time.deltaTime * 2);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(3f);
		while (story.color.a > 0){
			float newA = Mathf.Min(1f, story.color.a - Time.deltaTime);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		story.text = "The Council of Galactic Citizens determined that humanity should be wiped out, calling humans...";
		while (story.color.a < 1){
			float newA = Mathf.Min(1f, story.color.a + Time.deltaTime * 2);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(7.0f);
		while (story.color.a > 0){
			float newA = Mathf.Min(1f, story.color.a - Time.deltaTime);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		bold.text = "\"A monstrous threat.\"";
		while (bold.color.a < 1){
			float newA = Mathf.Min(1f, bold.color.a + Time.deltaTime);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(2f);
		while (bold.color.a > 0){
			float newA = Mathf.Min(1f, bold.color.a - Time.deltaTime * 2);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		story.text = "Now one ambassador has a few short minutes to\nconvince the council that humanity is civilized,\ncultured and worth saving.";
		while (story.color.a < 1){
			float newA = Mathf.Min(1f, story.color.a + Time.deltaTime * 2);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(6.0f);
		while (story.color.a > 0){
			float newA = Mathf.Min(1f, story.color.a - Time.deltaTime);
			story.color = new Color (story.color.r, story.color.g, story.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		bold.text = "This is your story.";
		while (bold.color.a < 1){
			float newA = Mathf.Min(1f, bold.color.a + Time.deltaTime);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(2.5f);
		while (bold.color.a > 0){
			float newA = Mathf.Min(1f, bold.color.a - Time.deltaTime * 2);
			bold.color = new Color (bold.color.r, bold.color.g, bold.color.b, newA);
			yield return null;
		}
		yield return new WaitForSeconds(2f);
		Application.LoadLevel(1);

	}
}
